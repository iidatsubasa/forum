package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
@RestController
public class ForumController {
	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	//@RequestParam(name="startDate") String startDate,@RequestParam(name="endDate") String endDate

	// 投稿内容表示画面
	@GetMapping("/")
	public ModelAndView top(@RequestParam(name="startDate", required = false) String startDate,@RequestParam(name="endDate", required = false) String endDate) throws ParseException{
		ModelAndView mav = new ModelAndView();

		List<Report> contentData = null;
		List<Comment> commentData = null;

		//投稿取得
		contentData = reportService.findReport(startDate, endDate);
		commentData = commentService.findAllComment();

		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		// 日付保管

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// createとupdateの時間をセット
		report.setCreatedDate(new Date());
		report.setUpdatedDate(new Date());
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		//UrlParameterのidをもとに投稿を削除
		reportService.deleteReport(id);
		 //rootへリダイレクト
		return new ModelAndView("redirect:/");

	}

	//編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		//編集する投稿を取得
		Report report = reportService.editReport(id);
		//編集する投稿をセット
		mav.addObject("formModel", report);
		//画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	//編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report){
		//UrlParameterのidを更新したentityにセット
		report.setId(id);
		report.setUpdatedDate(new Date());
		System.out.println(report.getUpdatedDate());
		//編集した投稿を更新
		reportService.saveReport(report);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}


	// コメント投稿処理
	@PostMapping("/reply")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		// createとupdateの時間をセット
		comment.setCreatedDate(new Date());
		comment.setUpdatedDate(new Date());
		// 投稿をテーブルに格納
		commentService.saveComment(comment);

		//紐づき投稿の更新日時更新
		Report report = new Report();
		report.setId(comment.getReportId());
		report.setUpdatedDate(comment.getUpdatedDate());
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント編集画面
	//編集画面
	@GetMapping("/editomment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		//編集する投稿を取得
		Comment comment = commentService.editComment(id);
		System.out.println(comment.getReportId());
		//編集する投稿をセット
		mav.addObject("formModel", comment);
		//画面遷移先を指定
		mav.setViewName("/editcomment");
		return mav;
	}

	//編集処理
	@PutMapping("/updatecomment/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment editcomment){
		//UrlParameterのidを更新したentityにセット
		Comment comment = commentService.editComment(id);
		comment.setContent(editcomment.getContent());
		comment.setUpdatedDate(new Date());
		//編集した投稿を更新
		commentService.saveComment(comment);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/deletecomment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		//UrlParameterのidをもとに投稿を削除
		commentService.deleteComment(id);
		 //rootへリダイレクト
		return new ModelAndView("redirect:/");

	}
}