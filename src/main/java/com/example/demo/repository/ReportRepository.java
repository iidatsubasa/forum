package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {


	List<Report> findByCreatedDateBetween(Sort by, Date start, Date end);

}