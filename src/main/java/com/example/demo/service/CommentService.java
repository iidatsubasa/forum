package com.example.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
@Transactional
public class CommentService {
	@Autowired
	CommentRepository commentRepository;
	// レコード全件取得
	public List<Comment> findAllComment() {

		return commentRepository.findAll(Sort.by(Sort.Direction.DESC, "updatedDate"));
	}

	// レコード追加
	public void saveComment(Comment comment) {
		System.out.println(comment.getContent());
		commentRepository.save(comment);
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}


}

