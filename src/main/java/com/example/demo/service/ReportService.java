package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;


@Service
@Transactional
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	public List<Report> findReport(String startDate, String endDate) throws ParseException {
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date start = null;
		Date end = null;
		if (startDate == null || startDate.isEmpty()) {
			startDate = "2020-01-01 00:00:00";
			start = sdFormat.parse(startDate);
		} else {
			startDate += " 00:00:00";
			start = sdFormat.parse(startDate);
		}
		if (endDate == null || endDate.isEmpty()) {
			end = new Date();
		} else {
			endDate += " 23:59:59";
			end = sdFormat.parse(endDate);
		}
		List<Report> report = reportRepository.findByCreatedDateBetween(Sort.by(Sort.Direction.DESC, "updatedDate"), start, end);
		return report;
	}
}
